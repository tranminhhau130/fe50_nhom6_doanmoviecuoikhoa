import React, { useState } from "react";
import loadingIMG from "../../assets/loading.png";
import "./style.scss";
export default function Loading() {
  const animation = () => {};

  const [rotate, setRotate] = useState("rotate(-15deg)");

  return (
    <div className="loading">
      <img src={loadingIMG} />
    </div>
  );
}
