import React, { useState } from "react";
import "./style.scss";
import logo from "../../assets/logo.png";
import option from "../../assets/menu-options.png";
import { Link } from "react-router-dom";
export default function Header() {
  const [translateX, setTranslate] = useState("translateX(100%)");
  const [overplay, setOverplay] = useState("none");
  function displayPhoneContent() {
    setTranslate("translateX(0%)");
    setOverplay("block");
  }

  function closePhoneContent() {
    setTranslate("translateX(100%)");
    setOverplay("none");
  }

  return (
    <div className="header-wrapper">
      <div className="header">
        <div className="header__logo">
          <Link to="/">
            <img src={logo} alt="" />
          </Link>
        </div>
        <div className="header__link">
          <ul>
            <a href="">Lịch Chiếu</a>
            <a href="">Cụm Rạp</a>
            <a href="">Tin Tức</a>
            <a href="">Ứng dụng</a>
          </ul>
        </div>
        <Link to="/login">
          <div className="header__account">
            <div className="header__account__logo">
              <img src="https://www.nhadatnhontrach.com/no-avatar.jpg" alt="" />
            </div>
            <span>Đăng nhập</span>
          </div>
        </Link>
      </div>

      <div className="header-phone">
        <div className="header-phone__logo">
          <a href="">
            <img src={logo} alt="" />
          </a>
        </div>
        <div
          className="overplay"
          style={{ display: `${overplay}` }}
          onClick={closePhoneContent}
        ></div>
        <div className="header-phone__option" onClick={displayPhoneContent}>
          <img src={option} alt="" />
        </div>
        <div
          className="header-phone__content"
          style={{ transform: `${translateX}` }}
        >
          <ul>
            <div className="header-phone__content__login">
              <Link to="/login">
                <div className="header__account">
                  <div className="header__account__logo">
                    <img
                      src="https://www.nhadatnhontrach.com/no-avatar.jpg"
                      alt=""
                    />
                  </div>
                  <span>Đăng nhập</span>
                </div>
              </Link>
              <div className="icon">
                <i
                  className="fa fa-angle-right"
                  onClick={closePhoneContent}
                ></i>
              </div>
            </div>
            <li>
              <a href="#">Lịch Chiếu</a>
            </li>
            <li>
              <a href="">Cụm Rạp</a>
            </li>
            <li>
              <a href="">Tin Tức</a>
            </li>
            <li>
              <a href="">Ứng Dụng</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}
