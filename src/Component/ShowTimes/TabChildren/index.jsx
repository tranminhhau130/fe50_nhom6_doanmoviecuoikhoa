import {
  Link,
  makeStyles,
  Tab,
  Tabs,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Button,
  Grid,
} from "@material-ui/core";
import React, { useState } from "react";
import { Fragment } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import format from "date-format";

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}
const useStyles = makeStyles((theme) => ({
  rootChildren: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: "flex",
    height: 700,
  },
  tabs: {
    width: "25%",
    border: "1px solid #ebebec",
  },
  tabPanel: {
    flex: 1,
    border: "1px solid #ebebec",
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  accordion: {
    padding: 15,
    boxShadow: "none",
    "&$expanded": { margin: 0 },
    "&::after": {
      content: "''",
      position: "absolute",
      bottom: 0,
      left: "50%",
      width: "calc(100% - 40px)",
      transform: "translateX(-50%)",
      borderBottom: "1px solid #ccc",
      borderColor: "rgba(238,238,238,.88)",
    },
    "&::before": {
      display: "none",
    },
  },
  accordionSummary: {
    margin: 0,
    alignItems: "center",
  },
  content: {
    // .MuiAccordionSummary-content.Mui-expanded
    "&$expanded": { margin: 0 },
    margin: 0,
    alignItems: "center",
  },
  expanded: {},
}));
export default function TabChild(props) {
  const classes = useStyles();
  const [value, setvalue] = useState(0);
  const handleChange = (event, newValue) => {
    setvalue(newValue);
  };
  const maHeThongRap = props.maHeThongRap;
  const cinemaListShowTime = props.cinemaListShowTime;

  function renderTabChildren() {
    return (
      <div className={classes.rootChildren}>
        <Tabs
          orientation="vertical"
          variant="scrollable"
          aria-label="Vertical tabs example"
          className={classes.tabs}
          value={value}
          onChange={handleChange}
          scrollButtons="off"
        >
          {renderTabChildrenDetail()}
        </Tabs>
        {renderTabPanelChildren(value)}
      </div>
    );
  }
  function renderTabChildrenDetail() {
    for (let item in cinemaListShowTime) {
      if (maHeThongRap === item && cinemaListShowTime[item].length) {
        return cinemaListShowTime[item].map((item) => {
          return (
            <Tab
              label={renderContentTab(item)}
              style={{ padding: 20, maxWidth: "100%", textAlign: "left" }}
              {...a11yProps(item.maCumRap)}
              key={item.maCumRap}
              className="showTimes__branch"
            ></Tab>
          );
        });
      }
    }
  }
  function renderContentTab(item) {
    return (
      <Fragment>
        <h2>{item.tenCumRap}</h2>
        <p>{item.diaChi}</p>
        <Link>Chi Tiết</Link>
      </Fragment>
    );
  }

  function renderShowTimeCinema(danhSachPhim) {
    let newListFilm = filterListFilm(danhSachPhim);
    console.log(newListFilm);
    return newListFilm.map((item, index) => {
      return (
        <Accordion
          classes={{
            root: classes.accordion,
            after: classes.after,
            before: classes.before,
            expanded: classes.expanded,
          }}
          defaultExpanded
          key={item.maPhim}
        >
          <AccordionSummary
            // makeStyle cho MuiAccordionSummary-root và MuiAccordionSummary-content
            // trong đó root bọc ngoài content
            classes={{
              root: classes.accordionSummary,
              content: classes.content,
              expanded: classes.expanded,
            }}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <img
              src={item.hinhAnh}
              style={{
                width: 50,
                float: "left",
                height: 50,
                objectFit: "cover",
              }}
              alt=""
            />
            <div
              style={{
                display: "block",
                overflow: "hidden",
                textOverflow: "ellipsis",
                flex: 1,
                marginLeft: 15,
              }}
            >
              <h5
                style={{
                  fontSize: 14,
                  color: "#000",
                  margin: 0,
                  lineHeight: "22px ",
                }}
              >
                {item.tenPhim}
              </h5>
            </div>
          </AccordionSummary>
          <AccordionDetails>
            <Grid container spacing={2}>
              {renderButtonShowTimeFilm(item.lstLichChieuTheoPhim)}
            </Grid>
          </AccordionDetails>
        </Accordion>
      );
    });
  }
  function renderButtonShowTimeFilm(lichChieu) {
    return lichChieu.map((item, index) => {
      return (
        <Grid item xs={6} sm={3}>
          <Button
            component="a"
            className="showTimes__film--button"
            variant="outlined"
            style={{
              padding: "5px 0",
              transition: "all 0.2s",
              textDecoration: "none",
              borderRadius: "7px",
              fontSize: "14px",
              color: "#9b9b9b",
              fontWeight: 400,
            }}
          >
            <span className="time">
              {format("hh:mm", new Date(item.ngayChieuGioChieu))}
            </span>
            <p className="date">
              ~ {format("dd/MM", new Date(item.ngayChieuGioChieu))}
            </p>
          </Button>
        </Grid>
      );
    });
  }
  function filterListFilm(danhSachPhim) {
    // filter film in 2020
    let resultShowTimeFilm = danhSachPhim.reduce((phim, item, index) => {
      item.lstLichChieuTheoPhim = item.lstLichChieuTheoPhim.reduce(
        (array, item, index) => {
          let year = format("yyyy", new Date(item.ngayChieuGioChieu));
          if (+year === 2020) {
            array.push(item);
          }
          return array;
        },
        []
      );
      if (item.lstLichChieuTheoPhim.length) {
        phim.push(item);
      }
      return phim;
    }, []);
    return resultShowTimeFilm;
  }
  function renderTabPanelChildren(value) {
    for (let item in cinemaListShowTime) {
      if (maHeThongRap === item && cinemaListShowTime[item].length) {
        return cinemaListShowTime[item].map((item, index) => {
          return (
            <div
              role="tabpanel"
              hidden={value !== index}
              id={`vertical-tabpanel-${item.maCumRap}`}
              aria-labelledby={`vertical-tab-${item.maCumRap}`}
              className={classes.tabPanel}
            >
              {value === index && (
                <Scrollbars style={{ height: "100%" }}>
                  {renderShowTimeCinema(item.danhSachPhim)}
                </Scrollbars>
              )}
            </div>
          );
        });
      }
    }
  }
  return <Fragment>{renderTabChildren()}</Fragment>;
}
