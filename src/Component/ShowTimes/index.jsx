import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchCinemaList,
  fetchCinemaShowTime,
} from "../../redux/actions/cinema.actions";
import "./style.scss";
import TabChildren from "./TabChildren";
import { Container } from "@material-ui/core";

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: "flex",
    height: 700,
    marginTop: 30,
    maxWidth: "940px",
    padding: 0,
  },
  tabs: {
    width: "10%",
    border: "1px solid #ebebec",
  },
  logo: {
    minWidth: "100%",
  },
}));

export default function ShowTimes() {
  const classes = useStyles();

  const [value, setvalue] = useState(0);
  const handleChange = (event, newValue) => {
    setvalue(newValue);
  };
  const cinemaList = useSelector((state) => {
    return state.cinema.cinemaList;
  });
  const cinemaListShowTime = useSelector((state) => {
    return state.cinema.cinemaListShowTime;
  });
  const dispatch = useDispatch();

  useEffect(function () {
    dispatch(fetchCinemaList());
    for (let item in cinemaListShowTime) {
      dispatch(fetchCinemaShowTime(item));
    }
  }, []);

  function imgLogo(props) {
    return (
      <img
        src={props.logo}
        alt={props.maHeThongRap}
        className="showTimes__logo--item"
      ></img>
    );
  }
  function renderLogo() {
    return cinemaList?.map((item, index) => {
      return (
        <Tab
          style={{ padding: 10 }}
          icon={imgLogo(item)}
          {...a11yProps(item.maHeThongRap)}
          key={item.maHeThongRap}
          classes={{ root: classes.logo }}
          className="showTimes__logo"
        />
      );
    });
  }
  function renderTabParent() {
    return cinemaList?.map((item, index) => {
      return (
        <div
          role="tabpanel"
          hidden={value !== index}
          id={`vertical-tabpanel-${item.maHeThongRap}`}
          aria-labelledby={`vertical-tab-${item.maHeThongRap}`}
          key={index}
          style={{ flex: 1 }}
        >
          {value === index && (
            <TabChildren
              maHeThongRap={item.maHeThongRap}
              cinemaList={cinemaList}
              cinemaListShowTime={cinemaListShowTime}
              key={item.maHeThongRap}
            ></TabChildren>
          )}
        </div>
      );
    });
  }

  return (
    <Container fixed className={classes.root}>
      <Tabs
        orientation="vertical"
        variant="scrollable"
        aria-label="Vertical tabs example"
        className={classes.tabs}
        value={value}
        onChange={handleChange}
      >
        {renderLogo()}
      </Tabs>
      {renderTabParent(value)}
    </Container>
  );
}
