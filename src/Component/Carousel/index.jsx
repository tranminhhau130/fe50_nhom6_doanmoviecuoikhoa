import React, { Component } from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";
import "./style.scss";
export default class DemoCarousel extends Component {
  render() {
    return (
      <Carousel autoPlay className="carousel">
        <div>
          <img src="https://s3img.vcdn.vn/123phim/2020/12/nghe-sieu-kho-16077632925121.jpg" />
        </div>
        <div>
          <img src="https://s3img.vcdn.vn/123phim/2020/11/bi-mat-cua-gio-sneakshow-16063182906605.jpg" />
        </div>
        <div>
          <img src="https://s3img.vcdn.vn/123phim/2020/12/ww84-16077617924558.jpg" />
        </div>
        <div>
          <img src="https://s3img.vcdn.vn/123phim/2020/11/bi-mat-cua-gio-sneakshow-16063182906605.jpg" />
        </div>
        <div>
          <img src="https://s3img.vcdn.vn/123phim/2020/11/gia-dinh-chan-to-phieu-luu-ky-bigfoot-family-p-16061896575573.png" />
        </div>
      </Carousel>
    );
  }
}
