import React from "react";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import "./style.scss";

export default function Cards(props) {
  const { image, href, title, description, type } = props.data;
  return (
    <Card className="card">
      <CardMedia
        component="a"
        alt="Contemplative Reptile"
        image={image}
        className={type === 2 ? "card__image medium" : "card__image"}
        href={href}
      />
      <CardContent className="card__content">
        <Typography
          gutterBottom
          variant="h5"
          component="a"
          href={href}
          className={
            type === 2 ? "card__content--title medium" : "card__content--title"
          }
        >
          {title}
        </Typography>
        <Typography
          className={
            type === 2 ? "card__content--text medium" : "card__content--text"
          }
          variant="body2"
          color="textSecondary"
          component="p"
        >
          {description}
        </Typography>
      </CardContent>

      <CardActions style={{ alignItems: "flex-start" }}>
        <div className="card__actions">
          <i className="fa fa-thumbs-up card__actions--icon"></i>
          <span className="card__actions--text">0</span>
        </div>
        <div className="card__actions">
          <i className="fa fa-comment card__actions--icon"></i>
          <span className="card__actions--text">0</span>
        </div>
      </CardActions>
    </Card>
  );
}
