import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import "./style.scss";

export default function CardSmall(props) {
  const { image, href, title } = props.data;

  return (
    <Card className="cardSmall">
      <CardMedia
        component="a"
        alt="Contemplative Reptile"
        image={image}
        className="cardSmall__image"
        href={href}
      />
      <CardContent className="cardSmall__content">
        <Typography
          gutterBottom
          variant="h5"
          component="a"
          href={href}
          className="cardSmall__content--title"
        >
          {title}
        </Typography>
      </CardContent>
    </Card>
  );
}
