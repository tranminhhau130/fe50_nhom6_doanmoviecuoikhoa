import React, { useState } from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { Box, Container, Typography } from "@material-ui/core";
import SwipeableViews from "react-swipeable-views";
import TabItem from "./TabItem";
// import "./style.scss";

function TabPanel(props) {
  const { children, value, index, dir, classes, ...other } = props;

  return (
    <div
      style={{ overflow: "unset" }}
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && <Box>{children}</Box>}
    </div>
  );
}
const useStyles = makeStyles((theme) => {
  return {
    root: {
      textAlign: "center",
      maxWidth: "940px",
      padding: 0,
      marginTop: "30px",
      marginBottom: "30px",
    },
    tab: {
      lineHeight: "24px",
      height: 24,
      backgroundColor: "transparent",
      border: "none",
      position: "relative",
      display: "block",
      padding: "10px 15px",
      fontSize: 20,
      transition: "all 0.2s",
      color: "#000000",
      opacity: 1,
      "&:hover": {
        fontSize: "24px",
        color: "red",
        marginRight: "5px",
        marginLeft: "5px",
      },
    },
    selected: {
      fontSize: "24px",
      color: "red",
      marginRight: "5px",
      marginLeft: "5px",
    },
    tabs: {
      marginBottom: "20px ",
    },
    indicator: {
      height: "0",
    },
  };
});

export default function News() {
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const theme = useTheme();
  const handleChange = (even, newValue) => {
    setValue(newValue);
  };

  const dataNews = [
    [
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/11/boc-tem-to-hop-giai-tri-moi-toanh-cua-gioi-ha-thanh-16056938333773.jpg",
        href:
          "https://tix.vn/goc-dien-anh/7960-boc-tem-to-hop-giai-tri-moi-toanh-cua-gioi-ha-thanh",
        title: "Bóc tem” tổ hợp giải trí mới toanh của giới Hà Thành",
        description:
          "Vào đúng ngày Nhà giáo Việt Nam 20/11, khu vui chơi sống ảo độc-lạ-chill nhất từ trước đến giờ sẽ chính thức khai trương tại 360 Giải Phóng! ",
        type: 1,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/11/tiec-trang-mau-chinh-thuc-can-moc-100-ty-chi-sau-2-tuan-cong-chieu-16043752411629.png",
        href:
          "https://tix.vn/goc-dien-anh/7957-tiec-trang-mau-chinh-thuc-can-moc-100-ty-chi-sau-2-tuan-cong-chieu",
        title:
          "Tiệc Trăng Máu chính thức cán mốc 100 tỷ chỉ sau 2 tuần công chiếu ",
        description:
          "Sau 2 tuần ra mắt, Tiệc Trăng Máu chính thức gia nhập câu lạc bộ phim điện ảnh đạt 100 tỷ đồng doanh thu phòng vé. Dàn ngôi sao “bạc tỷ” của phim cũng lần đầu tiên hội tụ đầy đủ trong một khung hình để ăn mừng thành tích ấn tượng của tác phẩm. ",

        type: 1,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/10/ngo-thanh-van-chinh-thuc-khoi-dong-cuoc-thi-thiet-ke-trang-phuc-cho-sieu-anh-hung-dau-tien-cua-viet-nam-vinaman-16041597587981.jpg",
        href:
          "https://tix.vn/goc-dien-anh/7956-ngo-thanh-van-chinh-thuc-khoi-dong-cuoc-thi-thiet-ke-trang-phuc-cho-sieu-anh-hung-dau-tien-cua-viet-nam-vinaman",
        title:
          "NGÔ THANH VÂN CHÍNH THỨC KHỞI ĐỘNG CUỘC THI THIẾT KẾ TRANG PHỤC CHO SIÊU ANH HÙNG ĐẦU TIÊN CỦA VIỆT NAM – VINAMAN",
        description:
          "Chiều tối ngày 30-10-2020, Ngô Thanh Vân và Studio68 đã chính thức phát động cuộc thi thiết kế trang phục cho siêu anh hùng VINAMAN với tổng giá trị giải thưởng lên đến 60 triệu đồng.",

        type: 2,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/11/antebellum-4-ly-do-khong-the-bo-lo-sieu-pham-kinh-di-antebellum-bay-thuc-tai-kinh-hoang-16045678023913.png",
        href:
          "https://tix.vn/goc-dien-anh/7953-antebellum-4-ly-do-khong-the-bo-lo-sieu-pham-kinh-di-antebellum-bay-thuc-tai-kinh-hoang",
        title:
          "[ANTEBELLUM] - 4 lý do không thể bỏ lỡ siêu phẩm kinh dị Antebellum: Bẫy Thực Tại Kinh Hoàng",
        description:
          "Không đi theo lối mòn máu me, hù dọa mà đầu tư khai thác những mảng tối của xã hội trên nền một câu chuyện kinh dị, có sự tham gia của nhà sản xuất đã làm nên thành công của loạt tác phẩm ấn tượng “Get Out”, “Us” hay “BlacKkKlansman”, và còn nhiều lý do khác khiến người yêu phim không thể bỏ lỡ Ante",
        type: 2,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/08/da-n-my-nhan-trong-the-gio-i-die-n-a-nh-cu-a-qua-i-kie-t-christopher-nolan-15970503793246.png",
        href:
          "https://tix.vn/goc-dien-anh/7952-ac-quy-doi-dau-soan-ngoi-peninsula-tro-thanh-phim-dung-dau-doanh-thu-tai-han-quoc-mua-dich",
        title:
          " Ác Quỷ Đối Đầu soán ngôi Peninsula, trở thành phim đứng đầu doanh thu tại Hàn Quốc mùa dịch",
        description: "",
        type: 3,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/08/da-n-my-nhan-trong-the-gio-i-die-n-a-nh-cu-a-qua-i-kie-t-christopher-nolan-15970503793246.png",
        href:
          "https://tix.vn/goc-dien-anh/7951-da-n-my-nhan-trong-the-gio-i-die-n-a-nh-cu-a-qua-i-kie-t-christopher-nolan",
        title:
          "Dàn mỹ nhân trong thế giới điện ảnh của quái kiệt Christopher Nolan",
        description: "",
        type: 3,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/08/truy-cung-giet-tan-cuoc-tai-ngo-cua-hai-ong-hoang-phong-ve-xu-han-15966122262210.png",
        href:
          "https://tix.vn/goc-dien-anh/7950-truy-cung-giet-tan-cuoc-tai-ngo-cua-hai-ong-hoang-phong-ve-xu-han",
        title:
          "Truy Cùng Giết Tận - Cuộc tái ngộ của hai 'ông hoàng phòng vé' xứ Hàn",
        description: "",
        type: 3,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/08/6-da-o-die-n-ti-do-lam-nen-thanh-cong-cua-nhu-ng-bom-ta-n-di-nh-da-m-nha-t-hollywood-15966023547553.png",
        href:
          "https://tix.vn/goc-dien-anh/7949-6-da-o-die-n-ti-do-lam-nen-thanh-cong-cua-nhu-ng-bom-ta-n-di-nh-da-m-nha-t-hollywood",
        title:
          "6 đạo diễn tỉ đô làm nên thành công của những bom tấn đình đám nhất Hollywood",
        description: "",
        type: 3,
      },
    ],
    [
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/08/gai-gia-lam-chieu-v-nhung-cuoc-doi-vuong-gia-15965999467501.png",
        href:
          "https://tix.vn/goc-dien-anh/7948-gai-gia-lam-chieu-v-nhung-cuoc-doi-vuong-gia",
        title: "Gái Già Lắm Chiêu V – Những cuộc đời vương giả",
        description:
          "Bộ phim qui tụ dàn diễn viên nổi tiếng nhiều thế hệ từ Bắc tới Nam, trong đó NSND Lê Khanh, NSND Hồng Vân sẽ tái hợp và đóng cùng nữ diễn viên trẻ Kaity Nguyễn để hóa thân thành ba chị em xinh đẹp nhà họ Lý giàu sang, nổi danh trong giới thượng lưu trí thức đương đại.  ",
        type: 1,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/07/kaity-nguyen-tro-thanh-my-nhan-moi-cua-vu-tru-gai-gia-lam-chieu-15959988971479.png",
        href:
          "https://tix.vn/goc-dien-anh/7945-kaity-nguyen-tro-thanh-my-nhan-moi-cua-vu-tru-gai-gia-lam-chieu",
        title:
          "Kaity Nguyễn trở thành mỹ nhân mới của vũ trụ Gái Già Lắm Chiêu",
        description:
          "Sau khi tiết lộ nhân tố đầu tiên là NSND Lê Khanh sẽ tiếp tục tham gia phần tiếp theo của Gái già lắm chiêu với mái tóc được cắt ngắn đầy mạnh mẽ và nam tính. Kaity Nguyễn sẽ là mỹ nhân tiếp theo nối gót hai đàn chị là Diễm My 9X và Ninh Dương Lan Ngọc gia nhập vũ trụ điện ảnh Gái Già Lắm Chiêu.",

        type: 1,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/07/5-ly-do-khien-ban-khong-the-bo-qua-bo-phim-cau-be-nguoi-go-pinocchio-15959331487131.png",
        href:
          "https://tix.vn/goc-dien-anh/7944-5-ly-do-khien-ban-khong-the-bo-qua-bo-phim-cau-be-nguoi-go-pinocchio",
        title:
          "5 lý do khiến bạn không thể bỏ qua bộ phim Cậu Bé Người Gỗ Pinocchio",
        description:
          "Không chỉ chuyển tải được giá trị của tác phẩm gốc, “Cậu Bé Người Gỗ Pinocchio” của năm 2020 còn thành công chinh phục thế hệ khán giả hiện đại với một trải nghiệm điện ảnh đầy sắc màu nhưng cũng vô cùng mới lạ. Cùng điểm qua 5 lý do khiến bộ phim đặc biệt đến vậy nhé!",

        type: 2,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/07/tenet-cong-bo-ngay-khoi-chieu-chinh-thuc-tai-viet-nam-15959320391357.png",
        href:
          "https://tix.vn/goc-dien-anh/7943-tenet-cong-bo-ngay-khoi-chieu-chinh-thuc-tai-viet-nam",
        title: "TENET công bố ngày khởi chiếu chính thức tại Việt Nam",
        description:
          "Đêm qua theo giờ Việt Nam, hãng phim Warner Bros. đưa ra thông báo chính thức về ngày khởi chiếu cho bom tấn TENET tại các thị trường bên ngoài Bắc Mỹ, trong đó có Việt Nam. ",
        type: 2,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/07/khi-phu-nu-khong-con-o-the-tron-chay-cua-nan-nhan-15943684395106.jpg",
        href:
          "https://tix.vn/goc-dien-anh/7941-khi-phu-nu-khong-con-o-the-tron-chay-cua-nan-nhan",
        title: "Khi phụ nữ không còn ở thế trốn chạy của nạn nhân",
        description: "",
        type: 3,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/07/gerard-butler-cung-bo-cu-deadpool-tham-gia-greenland-15937527518432.png",
        href:
          "https://tix.vn/goc-dien-anh/7940-gerard-butler-cung-bo-cu-deadpool-tham-gia-greenland",
        title: "Gerard Butler cùng bồ cũ Deadpool tham gia Greenland",
        description: "",
        type: 3,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/07/dien-vien-dac-biet-cua-bang-chung-vo-hinh-15937518582544.png",
        href:
          "https://tix.vn/goc-dien-anh/7939-dien-vien-dac-biet-cua-bang-chung-vo-hinh",
        title: "Diễn viên đặc biệt của Bằng Chứng Vô Hình",
        description: "",
        type: 3,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/07/pee-nak-2-van-kiep-thien-thu-di-tu-khong-het-nghiep-15937498464029.png",
        href:
          "https://tix.vn/goc-dien-anh/7938-pee-nak-2-van-kiep-thien-thu-di-tu-khong-het-nghiep",
        title: "Pee Nak 2 - Vạn kiếp thiên thu, đi tu không hết nghiệp!",
        description: "",
        type: 3,
      },
    ],
    [
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/07/loat-phim-kinh-di-khong-the-bo-lo-trong-thang-7-15937471008042.png",
        href:
          "https://tix.vn/goc-dien-anh/7937-loat-phim-kinh-di-khong-the-bo-lo-trong-thang-7",
        title: "Loạt phim kinh dị không thể bỏ lỡ trong tháng 7!",
        description:
          "Trong tháng 7 này, dòng phim kinh dị với các tác phẩm hấp dẫn hứa hẹn sẽ tung hoành khắp phòng vé và thỏa mãn sự đam mê tất cả khán giả yêu điện ảnh với những giây phút hồi hộp thót tim.",
        type: 1,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/06/rom-tung-trailer-he-lo-cuoc-song-cua-dan-choi-so-de-15929959650162.jpg",
        href:
          "https://tix.vn/goc-dien-anh/7936-rom-tung-trailer-he-lo-cuoc-song-cua-dan-choi-so-de",
        title: "RÒM tung trailer hé lộ cuộc sống của dân chơi số đề",
        description:
          "Với đề tài và góc nhìn mới lạ, RÒM hiện đang khiến giới mộ điệu và khán giả yêu điện ảnh nóng lòng chờ đợi ngày phim ra rạp. Nhà sản xuất mới đây đã tung poster và trailer chính thức, hé lộ những khung hình gay cấn và đậm chất nhân văn trong RÒM.",

        type: 1,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/06/antebellum-trailer-cuoi-cung-khong-he-lo-bat-cu-thong-tin-gi-them-15929866964476.jpg",
        href:
          "https://tix.vn/goc-dien-anh/7935-antebellum-trailer-cuoi-cung-khong-he-lo-bat-cu-thong-tin-gi-them",
        title:
          "Antebellum - Trailer cuối cùng không hé lộ bất cứ thông tin gì thêm",
        description:
          "Antebellum” - siêu phẩm kinh dị mới nhất đến từ nhà sản xuất của “Get Out” và “Us” nhá hàng trailer cuối, giữ nguyên chiến thuật “giấu giếm đến phút chót”, khơi gợi sự tò mò có phần hoang mang cho khán giả.",

        type: 2,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/06/ban-dao-peninsula-la-bom-tan-xac-song-khong-the-bo-lo-15925398181587.png",
        href:
          "https://tix.vn/goc-dien-anh/7934-ban-dao-peninsula-la-bom-tan-xac-song-khong-the-bo-lo",
        title: "Bán Đảo Peninsula là bom tấn xác sống không thể bỏ lỡ!",
        description:
          "Là phần phim khép lại bộ ba xác sống (Seoul Station, Train to Busan - 2016) của đạo diễn Yeon Sang Ho, mới đây, bom tấn Bán Đảo (Train to Busan 2/Peninsula) vừa chính thức tung trailer hé lộ những tình tiết mới cực hấp dẫn.",
        type: 2,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/06/toi-se-lam-tat-ca-ngo-ngang-boi-phien-ban-ta-ac-cua-minh-song-ji-hyo-15925375269303.png",
        href:
          "https://tix.vn/goc-dien-anh/7933-toi-se-lam-tat-ca-ngo-ngang-boi-phien-ban-ta-ac-cua-minh-song-ji-hyo",
        title:
          "‘Tôi sẽ làm tất cả ngỡ ngàng bởi phiên bản tà ác của mình’ - Song Ji Hyo",
        description: "",
        type: 3,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/06/hanh-trinh-cua-rom-va-cau-chuyen-dang-sau-de-tai-so-de-15925355281740.jpg",
        href:
          "https://tix.vn/goc-dien-anh/7932-hanh-trinh-cua-rom-va-cau-chuyen-dang-sau-de-tai-so-de",
        title: "Hành trình của Ròm và câu chuyện đằng sau đề tài số đề",
        description: "",
        type: 3,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/06/cung-on-lai-tinh-dau-voi-phim-dien-anh-kinh-dien-cua-chi-dep-son-ye-jin-duoc-thai-lan-lam-lai-15918642824994.jpg",
        href:
          "https://tix.vn/goc-dien-anh/7931-cung-on-lai-tinh-dau-voi-phim-dien-anh-kinh-dien-cua-chi-dep-son-ye-jin-duoc-thai-lan-lam-lai",
        title:
          "Cùng ôn lại 'Tình đầu' với phim điện ảnh kinh điển của chị đẹp Son Ye-jin được Thái Lan làm lại",
        description: "",
        type: 3,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/06/tam-quen-di-nhung-chu-bo-vui-ve-tren-nong-trai-ta-quay-cung-soi-nguyen-chat-100-15918628915499.jpg",
        href:
          "https://tix.vn/goc-dien-anh/7930-quay-cung-soi-nguyen-chat-100-co-suat-chieu-som-vao-20-va-21-06-2020",
        title:
          "Quẩy cùng sói 'nguyên chất 100%' có suất chiếu sớm vào 20 và 21.06.2020",
        description: "",
        type: 3,
      },
    ],
  ];
  const dataReview = [
    [
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/08/review-tan-tich-quy-am-relic-ba-the-he-va-moi-lien-ket-15965255784224.png",
        href:
          "https://tix.vn/review/7947-review-tan-tich-quy-am-relic-ba-the-he-va-moi-lien-ket",
        title: "Review: Tàn Tích Quỷ Ám (Relic) - Ba thế hệ và mối liên kết",
        description:
          "Điểm nhấn của phim kinh dị năm 2020 chính là Tàn Tích Quỷ Ám",
        type: 1,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/08/review-dinh-thu-oan-khuat-ghost-of-war-15965120886610.png",
        href:
          "https://tix.vn/review/7946-review-dinh-thu-oan-khuat-ghost-of-war",
        title: "Review: Dinh Thự Oan Khuất (Ghost Of War)",
        description:
          "Tuy là một bộ phim có chất lượng tốt, nhưng có vẻ Dinh Thự Oan Khuất vẫn chưa đủ để đem khán giả trở lại phòng vé!",
        type: 1,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/06/blackkklansman-coc-nuoc-lanh-de-nguoi-my-thuc-tinh-15910862294165.png",
        href:
          "https://tix.vn/review/7924-blackkklansman-coc-nuoc-lanh-de-nguoi-my-thuc-tinh",
        title: "‘BlacKkKlansman’ - cốc nước lạnh để người Mỹ thức tỉnh",
        description:
          "Tác phẩm nhận đề cử Phim truyện xuất sắc tại Oscar 2019 của đạo diễn Spike Lee là một lát cắt nữa về nạn phân biệt chủng tộc - nỗi đau gây nhức nhối nước Mỹ cho tới tận hôm nay.",

        type: 2,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/05/american-sniper-chinh-nghia-hay-phi-nghia-15905660338111.png",
        href:
          "https://tix.vn/review/7918-american-sniper-chinh-nghia-hay-phi-nghia",
        title: "American Sniper - Chính nghĩa hay phi nghĩa?",
        description:
          "American Sniper khắc họa một tay súng bắn tỉa “huyền thoại” của Hải quân Mỹ với 4 lần tham chiến ở Trung Đông. Câu chuyện phim chậm rãi đưa người xem qua từng thời khắc khác nhau của Kyle, từ thửa nhỏ, thiếu niên, rồi gia nhập quân đội, rồi tham chiến. Từng khoảnh khắc bắt đầu nhẹ nhàng...",
        type: 2,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/05/review-spider-man-into-the-spider-vesre-15886520889426.png",
        href:
          "https://tix.vn/review/7894-review-spider-man-into-the-spider-vesre",
        title: "Review: Spider-Man: Into The Spider-Vesre ",
        description: "",
        type: 3,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/03/covid-19-la-ban-chinh-thuc-cua-mev-1-phim-contagion-2011-15843496198482.jpg",
        href:
          "https://tix.vn/review/7886-covid-19-la-ban-chinh-thuc-cua-mev-1-phim-contagion-2011",
        title: "COVID-19 là bản chính thức của MEV-1 phim contagion (2011)",
        description: "",
        type: 3,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/03/review-sieu-ve-si-so-vo-giai-cuu-tong-thong-chua-bao-gio-lay-loi-va-hai-huoc-den-the-15840925506832.jpg",
        href:
          "https://tix.vn/review/7882-review-sieu-ve-si-so-vo-giai-cuu-tong-thong-chua-bao-gio-lay-loi-va-hai-huoc-den-the",
        title:
          "[Review] Siêu Vệ Sĩ Sợ Vợ - Giải cứu Tổng thống chưa bao giờ lầy lội và hài hước đến thế",
        description: "",
        type: 3,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/03/review-bloodshot-mo-man-an-tuong-cho-vu-tru-sieu-anh-hung-valiant-15840731141389.jpg",
        href:
          "https://tix.vn/review/7881-review-bloodshot-mo-man-an-tuong-cho-vu-tru-sieu-anh-hung-valiant",
        title:
          "[Review] Bloodshot - Mở màn ấn tượng cho Vũ trụ Siêu anh hùng Valiant",
        description: "",
        type: 3,
      },
    ],
    [
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/03/review-nang-3-loi-hua-cua-cha-cau-chuyen-tinh-than-cam-dong-cua-kha-nhu-va-kieu-minh-tuan-15834049872311.jpg",
        href:
          "https://tix.vn/review/7876-review-nang-3-loi-hua-cua-cha-cau-chuyen-tinh-than-cam-dong-cua-kha-nhu-va-kieu-minh-tuan",
        title:
          "[Review] Nắng 3: Lời Hứa Của Cha - Câu chuyện tình thân cảm động của Khả Như và Kiều Minh Tuấn",
        description:
          "Như hai phần phim trước, Nắng 3 tiếp tục mang đến câu chuyện tình cha, mẹ - con đầy nước mắt của bộ ba nhân vật chính.",
        type: 1,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/03/review-onward-khi-phep-thuat-manh-me-nhat-chinh-la-tinh-than-15832047938817.jpg",
        href:
          "https://tix.vn/review/7871-review-onward-khi-phep-thuat-manh-me-nhat-chinh-la-tinh-than",
        title:
          "[Review] Onward - Khi phép thuật mạnh mẽ nhất chính là tình thân",
        description:
          "Tác phẩm mới nhất của Pixar tiếp tục là câu chuyện hài hước và cảm xúc về tình cảm gia đình.",

        type: 1,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/02/review-ke-vo-hinh-con-gi-dang-so-hon-ke-giet-nguoi-benh-hoan-vo-hinh-15828835353362.jpg",
        href:
          "https://tix.vn/review/7868-review-ke-vo-hinh-con-gi-dang-so-hon-ke-giet-nguoi-benh-hoan-vo-hinh",
        title:
          "[Review] Kẻ Vô Hình - Còn gì đáng sợ hơn kẻ giết người bệnh hoạn vô hình?",
        description:
          "Phiên bản hiện đại của The Invisible Man là một trong những phim kinh dị xuất sắc nhất năm nay.",

        type: 2,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/02/review-cau-be-ma-2-ban-trai-cua-be-beo-la-day-chu-dau-xa-15823608583110.jpg",
        href:
          "https://tix.vn/review/7861-review-cau-be-ma-2-ban-trai-cua-be-beo-la-day-chu-dau-xa",
        title: "[Review] Cậu Bé Ma 2 - Bạn trai của 'bé Beo' là đây chứ đâu xa",
        description:
          "Brahms: The Boy II có những màn hù dọa ấn tượng nhưng cái kết lại không tương xứng với phần mở đầu hứa hẹn.",
        type: 2,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/02/review-nhim-sonic-cuoi-tha-ga-cung-chang-nhim-sieu-thanh-lay-loi-15821907793369.jpg",
        href:
          "https://tix.vn/review/7859-review-nhim-sonic-cuoi-tha-ga-cung-chang-nhim-sieu-thanh-lay-loi",
        title:
          "[Review] Nhím Sonic - Cười thả ga cùng chàng nhím siêu thanh lầy lội",
        description: "",
        type: 3,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/02/review-thang-nam-hanh-phuc-ta-tung-co-buong-bo-chua-bao-gio-la-viec-de-dang-15817967038683.jpg",
        href:
          "https://tix.vn/review/7858-review-thang-nam-hanh-phuc-ta-tung-co-buong-bo-chua-bao-gio-la-viec-de-dang",
        title:
          "[Review] Tháng Năm Hạnh Phúc Ta Từng Có - Buông bỏ chưa bao giờ là việc dễ dàng",
        description: "",
        type: 3,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/02/review-sac-dep-doi-tra-huong-giang-ke-chuyen-doi-minh-qua-phim-anh-15817958389162.jpg",
        href:
          "https://tix.vn/review/7857-review-sac-dep-doi-tra-huong-giang-ke-chuyen-doi-minh-qua-phim-anh",
        title:
          "[Review] Sắc Đẹp Dối Trá - Hương Giang kể chuyện đời mình qua phim ảnh",
        description: "",
        type: 3,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/02/review-birds-of-prey-15809871977193.jpg",
        href:
          "https://tix.vn/review/7852-review-birds-of-prey-man-lot-xac-hoanh-trang-cua-harley-quinn-va-dc",
        title:
          "[Review] Birds of Prey - Màn lột xác hoành tráng của Harley Quinn và DC",
        description: "",
        type: 3,
      },
    ],
    [
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/02/review-bi-mat-cua-gio-cau-chuyen-tinh-nguoi-duyen-ma-day-nuoc-mat-15806427466175.jpg",
        href:
          "https://tix.vn/review/7847-review-bi-mat-cua-gio-cau-chuyen-tinh-nguoi-duyen-ma-day-nuoc-mat",
        title:
          "[Review] Bí Mật Của Gió - Câu chuyện “tình người duyên ma” đầy nước mắt",
        description:
          "Sau 5 năm, đạo diễn Nguyễn Phan Quang Bình đã trở lại với một câu chuyện tình đẹp nhưng nhiều day dứt và tràn đầy ý nghĩa.",
        type: 1,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/01/gai-gia-lam-chieu-3-cuoc-chien-me-chong-nang-dau-cua-gioi-sieu-giau-xu-hue-15798623549864.jpg",
        href:
          "https://tix.vn/review/7845-review-gai-gia-lam-chieu-3-cuoc-chien-me-chong-nang-dau-cua-gioi-sieu-giau-xu-hue",
        title:
          "[Review] Gái Già Lắm Chiêu 3 - Cuộc chiến mẹ chồng - nàng dâu của giới siêu giàu xứ Huế",
        description:
          "Tác phẩm mới nhất của bộ đôi đạo diễn Bảo Nhân và Nam Cito mang đến nhiều tiếng cười và ý nghĩa. Đừng để trailer lừa bạn như một tác phẩm nào đó nhé.",

        type: 1,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/01/30-chua-phai-tet-phim-vong-lap-thoi-gian-thuong-hieu-viet-15797534839619.jpg",
        href:
          "https://tix.vn/review/7844-review-30-chua-phai-tet-phim-vong-lap-thoi-gian-thuong-hieu-viet",
        title:
          "[Review] 30 Chưa Phải Tết - Phim vòng lặp thời gian thương hiệu Việt",
        description:
          "30 Chưa Phải Tết là tác phẩm đầu tiên khai thác đề tài vòng lặp thời gian với sự kết hợp giữa Quang Huy và Trường Giang.",
        type: 2,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/01/doi-mat-am-duong-mon-la-dau-nam-vua-hai-vua-so-cua-dien-anh-viet-15795913229687.jpg",
        href:
          "https://tix.vn/review/7841-review-doi-mat-am-duong-mon-la-dau-nam-vua-hai-vua-so-cua-dien-anh-viet",
        title:
          "[Review] Đôi Mắt Âm Dương - Món lạ đầu năm vừa hài vừa sợ của điện ảnh Việt",
        description:
          "Chọn thể loại kinh dị ra mắt trong dịp Tết Nguyên đán như Đôi Mắt Âm Dương là một nước đi đầy táo bạo của Nhất Trung.",
        type: 2,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/01/bad-boys-for-life-khi-gung-cang-gia-cang-nhay-15791725156210.jpg",
        href:
          "https://tix.vn/review/7839-review-bad-boys-for-life-khi-gung-cang-gia-cang-nhay",
        title: "[Review] Bad Boys For Life - Khi gừng càng già càng… nhây",
        description: "",
        type: 3,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2020/01/underwater-noi-kinh-hoang-duoi-day-bien-xanh-sau-tham-15786347607632.jpg",
        href:
          "https://tix.vn/review/7835-review-underwater-noi-kinh-hoang-duoi-day-bien-xanh-sau-tham",
        title:
          "[Review] Underwater - Nỗi kinh hoàng dưới đáy biển xanh sâu thẵm",
        description: "",
        type: 3,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2019/12/diep-vien-an-danh-sieu-pham-hoat-hinh-lay-loi-dip-cuoi-nam-15774343881631.jpg",
        href:
          "https://tix.vn/review/7828-review-diep-vien-an-danh-sieu-pham-hoat-hinh-lay-loi-dip-cuoi-nam",
        title:
          "[Review] Điệp Viên Ẩn Danh - Siêu phẩm hoạt hình lầy lội dịp cuối năm",
        description: "",
        type: 3,
      },
      {
        image:
          "https://s3img.vcdn.vn/123phim/2019/12/mat-biec-tinh-dau-cu-ngo-mot-thoi-ai-ngo-day-dut-mot-doi-15770445440752.jpg",
        href:
          "https://tix.vn/review/7821-review-mat-biec-tinh-dau-cu-ngo-mot-thoi-ai-ngo-day-dut-mot-doi",
        title:
          "[Review] Mắt Biếc - Tình đầu cứ ngỡ một thời, ai ngờ day dứt một đời!",
        description: "",
        type: 3,
      },
    ],
  ];

  return (
    <Container className={classes.root}>
      <Tabs
        value={value}
        onChange={handleChange}
        indicatorColor="primary"
        centered
        classes={{
          root: classes.tabs,
          indicator: classes.indicator,
        }}
      >
        <Tab
          disableRipple
          classes={{
            textColorInherit: classes.tab,
            selected: classes.selected,
          }}
          label="Điện Ảnh 24h"
        />
        <Tab
          disableRipple
          classes={{
            textColorInherit: classes.tab,
            selected: classes.selected,
          }}
          label="Review"
        />
        <Tab
          disableRipple
          classes={{
            textColorInherit: classes.tab,
            selected: classes.selected,
          }}
          label="Khuyến Mãi"
        />
      </Tabs>
      <Typography>
        <SwipeableViews
          axis={theme.direction === "rtl" ? "x-reverse" : "x"}
          index={value}
          onChange={handleChange}
          slideStyle={{ overflow: "unset" }}
          containerStyle={{ overflow: "unset" }}
        >
          <TabPanel value={value} index={0} dir={theme.direction}>
            <TabItem data={dataNews}></TabItem>
          </TabPanel>
          <TabPanel value={value} index={1} dir={theme.direction}>
            <TabItem data={dataReview}></TabItem>
          </TabPanel>
          <TabPanel value={value} index={2} dir={theme.direction}>
            <TabItem data={dataNews}></TabItem>
          </TabPanel>
        </SwipeableViews>
      </Typography>
    </Container>
  );
}
