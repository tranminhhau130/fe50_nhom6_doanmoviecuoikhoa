import { Button, Card, Grid } from "@material-ui/core";
import React, { useState } from "react";
import { Fragment } from "react";
import Cards from "../Cards";
import CardSmall from "../Cards/CardSmall";

export default function TabItem(props) {
  const [amouth, setAmouth] = useState(1);
  const handleChangeAmouth = () => {
    setAmouth(amouth + 1);
  };
  const data = props.data;
  const length = data.length;
  function renderCard(data) {
    return data.map((item, index) => {
      const { type } = item;
      if (type === 1) {
        return (
          <Grid item xs={6}>
            <Cards data={item}></Cards>
          </Grid>
        );
      }
      return (
        <Grid item xs={4}>
          <Cards data={item}></Cards>
        </Grid>
      );
    });
  }
  function renderContentTabItem() {
    return data.map((item, index) => {
      const newDataItem = item.filter((item, index) => {
        return item.type !== 3;
      });
      const newDataItemSmall = item.filter((item, index) => {
        return item.type === 3;
      });
      if (+index < amouth) {
        return (
          <Grid
            spacing={2}
            justify="center"
            container
            style={{ marginBottom: "10px" }}
          >
            {renderCard(newDataItem)}
            <Grid item xs={4}>
              {renderCardSmall(newDataItemSmall)}
            </Grid>
          </Grid>
        );
      }
    });
  }
  function renderCardSmall(data) {
    return data.map((card, index) => {
      const { type } = card;
      return (
        <Fragment>
          <CardSmall data={card}></CardSmall>
        </Fragment>
      );
    });
  }
  return (
    <Fragment>
      {renderContentTabItem()}
      {length !== amouth && (
        <Button
          color="primary"
          variant="outlined"
          onClick={handleChangeAmouth}
          style={{ margin: "30px 0" }}
        >
          Xem Thêm
        </Button>
      )}
    </Fragment>
  );
}
