import React, { Component } from "react";
import Carousel from "../../Component/Carousel";
import Header from "../../Component/Header";
import ShowTimes from "../../Component/ShowTimes";

import Loading from "../../Component/Loading";
import "./style.scss";
import News from "../../Component/News";
class Home extends Component {
  render() {
    return (
      <div className="">
        <Header />
        <Carousel />
        <ShowTimes></ShowTimes>
        <News></News>
      </div>
    );
  }
}

export default Home;
