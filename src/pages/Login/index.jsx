import React, { useState } from "react";
import "./style.scss";
import background from "../../assets/bg-login.jpg";
import logo from "../../assets/login-logo.png";
import { Button } from "@material-ui/core";
import Axios from "axios";
import { useHistory } from "react-router-dom";
export default function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [warning, setWarning] = useState("");
  const history = useHistory();

  const changeUsername = (event) => {
    setUsername(event.target.value);
  };

  const changePassword = (event) => {
    setPassword(event.target.value);
  };

  const login = () => {
    Axios({
      url: "https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap",
      method: "POST",
      data: {
        taiKhoan: username,
        matKhau: password,
      },
    })
      .then((res) => {
        console.log(res);
        setWarning("");
        localStorage.setItem("userTix", res.data);
        history.push("/");
      })
      .catch((err) => {
        setWarning("*Tài khoản Hoặc mật khẩu không đúng");
      });
  };

  return (
    <div className="login" style={{ backgroundImage: `url(${background})` }}>
      <div className="login__form">
        <div className="login__form__logo">
          <img src={logo} alt="" />
        </div>
        <form>
          <div className="login__username">
            <i className="fa fa-user"></i>
            <input
              type="text"
              placeholder="Username"
              onChange={(event) => {
                changeUsername(event);
              }}
            />
          </div>
          <br />
          <div className="login__password">
            <i className="fa fa-key"></i>
            <input
              type="Password"
              placeholder="Password"
              onChange={(event) => {
                changePassword(event);
              }}
            />
          </div>
          <p className="warning">{warning}</p>
          <div className="login__button">
            <Button
              variant="contained"
              color="default"
              className="button"
              onClick={login}
            >
              Log in
            </Button>
          </div>
        </form>
      </div>
    </div>
  );
}
