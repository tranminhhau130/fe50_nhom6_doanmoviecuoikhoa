import { combineReducers } from "redux";
import movieReducer from "./movie.reducer";
import ticketReducer from "./ticket.reducer";
import userReducer from "./user.reducer";
import cinemaReducer from "./cinema.reducer";

const rootReducer = combineReducers({
  movie: movieReducer,
  ticket: ticketReducer,
  user: userReducer,
  cinema: cinemaReducer,
});

export default rootReducer;
