let initialState = {
  isLogin: false,
  data: {},
};

const userReducer = (state = initialState, action) => {
  let { type, payload } = action;
  switch (type) {
    default:
      return state;
  }
};
export default userReducer;
